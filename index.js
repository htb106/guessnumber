const guessNumber = Math.round(Math.random() * 10);
console.log(guessNumber)
const numberHistory = [];

const checkNumberInput = () =>{
    const numberInput =  document.querySelector("#numberInput").value;
    numberHistory.push(numberInput);
    if(numberInput == guessNumber){
        document.querySelector("#notification").innerHTML = "Chúc mừng bạn đã đoán đúng số";
        renderHistory()
    } else if(numberInput > guessNumber){
        document.querySelector("#notification").innerHTML = "Số bạn nhập vào lớn hơn số cần tìm";
    } else if(numberInput < guessNumber){
        document.querySelector("#notification").innerHTML = "Số bạn nhập vào nhỏ hơn số cần tìm";
    }
};

const clearInput = () => {
    document.querySelector("#numberInput").value = "";
    document.querySelector("#notification").innerHTML = "";
}

const renderHistory = () => {
    let content = "";
    for(let i = 0; i < numberHistory.length; i++){
        content += `
            <p>Lần ${i + 1}: ${numberHistory[i]}</p>
        `
    }
    document.querySelector("#content").innerHTML = content;
}

document.querySelector("#btnRandom").addEventListener("click", checkNumberInput);
document.querySelector("#numberInput").addEventListener("click", clearInput);